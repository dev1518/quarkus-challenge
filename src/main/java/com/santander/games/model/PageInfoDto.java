package com.santander.games.model;

import java.io.Serializable;

public class PageInfoDto implements Serializable {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = -4538049793515988272L;

	private int number;
	private int size;
	private int numberOfElements;
	private long totalElements;
	private int totalPages;
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	public int getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public long getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	public int getTotalPages() {
		return totalPages;
	}
	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
}
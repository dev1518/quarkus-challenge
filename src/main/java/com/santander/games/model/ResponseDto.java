package com.santander.games.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResponseDto <T> implements Serializable {

	/**
	 * serial number
	 */
	@JsonIgnore
	private static final long serialVersionUID = 1149759304331108073L;
	
	private String message;
    private int code;
    private T content;
    private List<String> errors;
    
    /**
     * Agrega un nuevo error a la lista de errores
     * @param error nuevo error a agregar
     */
    public void addError(String error) {
    	if(null == errors && null != error) {
    		this.errors = new ArrayList<>();
    	}
    	if(null != error) {
    		this.errors.add(error);
    	}
    }
    
    @JsonIgnore
    public boolean isOk() {
    	return 0 == code;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode(){
        return this.code;
    }

    public List<String> getErrors() {
        return this.errors;
    }

    public void setContent(T content){
        this.content = content;
    }

    public T getContent(){
        return this.content;
    }
    
}
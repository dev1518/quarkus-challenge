package com.santander.games.model;


public class ResponsePagedDto<T> extends ResponseDto<T> {

	/**
	 * serial version
	 */
	private static final long serialVersionUID = 1L;
	
	private PageInfoDto pageInfo;
	
	public ResponsePagedDto(){
		super();
	}

	public PageInfoDto getPageInfo() {
		return pageInfo;
	}

	public void setPageInfo(PageInfoDto pageInfo) {
		this.pageInfo = pageInfo;
	}

}
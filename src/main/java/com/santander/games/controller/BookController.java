package com.santander.games.controller;


import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.santander.games.entity.Book;
import com.santander.games.model.ResponseDto;
import com.santander.games.service.BookService;

import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("books")
public class BookController {

    @Inject
    private BookService service;

    /**
     * Consulta de todos los registros de la bd
     * @return respuesta obtenida
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response all() {
        return Response.ok(service.getAll()).build();
    }

    /**
     * Consulta de registros de la bd paginados
     * @return respuesta obtenida
     */
    @GET
    @Path("all/{page}/{size}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response all(@PathParam int page, @PathParam int size) {
        ResponseDto<?> response = service.getAll(page, size);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @GET
    @Path("{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByName(@PathParam String name){
        ResponseDto<?> response = service.findByName(name);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @GET
    @Path("{name}/{page}/{size}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByName(@PathParam String name, @PathParam int page, @PathParam int size){
        ResponseDto<?> response = service.findByName(name, page, size);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @GET
    @Path("{lowerYear}/{higherYear}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByYear(@PathParam Integer lowerYear, @PathParam Integer higherYear){
        ResponseDto<?> response = service.findByYear(lowerYear, higherYear);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @GET
    @Path("{lowerYear}/{higherYear}/{page}/{size}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getByYear(@PathParam Integer lowerYear, @PathParam Integer higherYear, @PathParam int page, @PathParam int size){
        ResponseDto<?> response = service.findByYear(lowerYear, higherYear, page, size);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response add(Book book){
        return Response
            .status(Status.CREATED)
            .entity(service.save(book))
            .build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response update(Book book){
        ResponseDto<?> response = service.update(book);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(Book book){
        ResponseDto<?> response = service.delete(book);
        if(response.isOk()){
            return Response.ok(response).build();
        }
        return Response
            .status(Status.NOT_FOUND)
            .entity(response)
            .build();
    }

}
package com.santander.games.service;

import java.util.List;

import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.santander.games.entity.Book;
import com.santander.games.model.PageInfoDto;
import com.santander.games.model.ResponseDto;
import com.santander.games.model.ResponsePagedDto;
import com.santander.games.repository.BookRepository;
import com.santander.games.utils.ResponseManager;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

@ApplicationScoped
public class BookService {

    @Inject
    private BookRepository repo;

    public ResponseDto<?> getAll(){
        ResponseDto<Iterable<Book>> response = new ResponseDto<>();
        Iterable<Book> it = repo.findAll();
        ResponseManager.manageSuccess(response);
        response.setContent(it);
        return response;
    }

    public ResponseDto<?> getAll(int page, int size){
        ResponsePagedDto<List<Book>> response = new ResponsePagedDto<>();
        Page<Book> pageList = repo.findAll(PageRequest.of(page, size));
        if(pageList.getSize() > 0){
            response.setContent(pageList.getContent());
            PageInfoDto pageInfo = new PageInfoDto();
            pageInfo.setNumber(pageList.getNumber());
            pageInfo.setNumberOfElements(pageList.getNumberOfElements());
            pageInfo.setSize(pageList.getSize());
            pageInfo.setTotalElements(pageList.getTotalElements());
            pageInfo.setTotalPages(pageList.getTotalPages());
            response.setPageInfo(pageInfo);
            ResponseManager.manageSuccess(response);
        } else {
            ResponseManager.manageException(response);
        }
        return response;
    }

    public ResponseDto<?> findByName(String name){
        ResponseDto<List<Book>> response = new ResponseDto<>();
        List<Book> list = repo.findByNameContainingIgnoreCase(name);
        if( ! list.isEmpty()){
            ResponseManager.manageSuccess(response);
            response.setContent(list);
        } else{
            ResponseManager.manageException(response);
        }
        return response;
    }

    public ResponseDto<?> findByName(String name, int page, int size){
        ResponsePagedDto<List<Book>> response = new ResponsePagedDto<>();
        Page<Book> pageList = repo.findByNameContainingIgnoreCase(name, PageRequest.of(page, size));
        if(pageList.getContent().size() > 0){
            response.setContent(pageList.getContent());
            PageInfoDto pageInfo = new PageInfoDto();
            pageInfo.setNumber(pageList.getNumber());
            pageInfo.setNumberOfElements(pageList.getNumberOfElements());
            pageInfo.setSize(pageList.getSize());
            pageInfo.setTotalElements(pageList.getTotalElements());
            pageInfo.setTotalPages(pageList.getTotalPages());
            response.setPageInfo(pageInfo);
            ResponseManager.manageSuccess(response);
        } else{
            ResponseManager.manageException(response);
        }
        return response;
    }

    public ResponseDto<?> findByYear(Integer lowerYear, Integer higherYear){
        ResponseDto<List<Book>> response = new ResponseDto<>();
        List<Book> list = repo.findByPublicationYearBetweenOrderByPublicationYearAsc(lowerYear, higherYear);
        if( ! list.isEmpty()){
            ResponseManager.manageSuccess(response);
            response.setContent(list);
        } else{
            ResponseManager.manageException(response);
        }
        return response;
    }

    public ResponseDto<?> findByYear(Integer lowerYear, Integer higherYear, int page, int size){
        ResponsePagedDto<List<Book>> response = new ResponsePagedDto<>();
        Page<Book> pageList = repo.findByPublicationYearBetweenOrderByPublicationYearAsc(lowerYear, higherYear, PageRequest.of(page, size));
        if(pageList.getContent().size() > 0){
            response.setContent(pageList.getContent());
            PageInfoDto pageInfo = new PageInfoDto();
            pageInfo.setNumber(pageList.getNumber());
            pageInfo.setNumberOfElements(pageList.getNumberOfElements());
            pageInfo.setSize(pageList.getSize());
            pageInfo.setTotalElements(pageList.getTotalElements());
            pageInfo.setTotalPages(pageList.getTotalPages());
            response.setPageInfo(pageInfo);
            ResponseManager.manageSuccess(response);
        } else{
            ResponseManager.manageException(response);
        }
        return response;
    }

    public ResponseDto<?> save(Book book){
        ResponseDto<Book> response = new ResponseDto<>();
        Book result = repo.save(book);
        response.setContent(result);
        ResponseManager.manageSuccess(response);
        return response;
    }

    public ResponseDto<?> update(Book book){
        ResponseDto<Book> response = new ResponseDto<>();
        if(Objects.nonNull(book) && Objects.nonNull(book.getId())){
            if(repo.existsById(book.getId())){
                response.setContent(repo.save(book));
                ResponseManager.manageSuccess(response);
                return response;
            }
        }
        response.setContent(book);
        ResponseManager.manageException(response);
        return response;
    } 

    public ResponseDto<?> delete(Book book){
        ResponseDto<Book> response = new ResponseDto<>();
        response.setContent(book);
        if(Objects.nonNull(book) && Objects.nonNull(book.getId())){
            if(repo.existsById(book.getId())){
                repo.deleteById(book.getId());
                ResponseManager.manageSuccess(response);
                return response;
            }
        }
        ResponseManager.manageException(response);
        return response;
    }  

}
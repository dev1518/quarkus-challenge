package com.santander.games.utils;

import com.santander.games.model.ResponseDto;

public final class ResponseManager {
	
	/**
	 * Constructor privado para no permitir que se creen objetos de esta clase
	 */
	private ResponseManager() {}
	
	/**
	 * Agrega codigo de exito a una respuesta dada
	 * @param response response
	 */
	public static void manageSuccess(ResponseDto<?> response) {
		response.setCode(0);
		response.setMessage("SUCCESS");
	}
	
	/**
	 * Agrega codigo de error a una respuesta dada
	 * @param response response
	 */
	public static void manageException(ResponseDto<?> response) {
		response.setCode(-1);
		response.setMessage("ERROR");
		response.addError("ERROR_MESSAGE");
	}

}
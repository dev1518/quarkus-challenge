package com.santander.games.repository;

import java.util.List;

import com.santander.games.entity.Book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Integer> {

    Page<Book> findAll(Pageable pageable);

    List<Book> findByNameContainingIgnoreCase(String name);

    Page<Book> findByNameContainingIgnoreCase(String name, Pageable pageable);

    List<Book> findByPublicationYearBetweenOrderByPublicationYearAsc(Integer lowerYear, Integer higherYear);

    Page<Book> findByPublicationYearBetweenOrderByPublicationYearAsc(Integer lowerYear, Integer higherYear, Pageable pageable);

}